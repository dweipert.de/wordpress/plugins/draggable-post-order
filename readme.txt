=== Draggable Post Order ===
Contributors: drogueronin
Tags: post-type, custom-post-type, order, drag
Requires at least: 5.5
Tested up to: 5.7
Stable tag: 1.0.3
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Drag'n'drop posts to order them

== Description ==

### Features

- makes post types orderable in the admin screen via drag'n'drop with *jquery-ui-sortable*

### How to use

Add this to your **functions.php**

`
add_post_type_support('your-custom-post-type', \Draggable_Post_Order\Draggable_Post_Order::$post_type_feature);
`
