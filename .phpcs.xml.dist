<?xml version="1.0"?>
<ruleset name="WordPress Coding Standards based custom ruleset for your plugin">
	<description>Generally-applicable sniffs for WordPress plugins.</description>

	<!-- What to scan -->
	<file>.</file>
	<exclude-pattern>/build/</exclude-pattern>
	<exclude-pattern>/node_modules/</exclude-pattern>
	<exclude-pattern>/tests/bootstrap.php</exclude-pattern>
	<exclude-pattern>/vendor/</exclude-pattern>

	<!-- How to scan -->
	<!-- Usage instructions: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Usage -->
	<!-- Annotated ruleset: https://github.com/squizlabs/PHP_CodeSniffer/wiki/Annotated-ruleset.xml -->
	<arg value="sp"/> <!-- Show sniff and progress -->
	<arg name="basepath" value="./"/><!-- Strip the file paths down to the relevant bit -->
	<arg name="colors"/>
	<arg name="extensions" value="php"/>
	<arg name="parallel" value="8"/><!-- Enables parallel processing when available for faster results. -->

	<!-- Rules: Check PHP version compatibility -->
	<!-- https://github.com/PHPCompatibility/PHPCompatibility#sniffing-your-code-for-compatibility-with-specific-php-versions -->
	<config name="testVersion" value="7.4-"/>
	<!-- https://github.com/PHPCompatibility/PHPCompatibilityWP -->
	<rule ref="PHPCompatibilityWP"/>

	<!-- Rules: WordPress Coding Standards -->
	<!-- https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards -->
	<!-- https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/wiki/Customizable-sniff-properties -->
	<config name="minimum_supported_wp_version" value="4.6"/>
	<rule ref="WordPress"/>
	<rule ref="WordPress.NamingConventions.PrefixAllGlobals">
		<properties>
			<property name="prefixes" type="array" value="draggable_post_order"/>
		</properties>
	</rule>
	<rule ref="WordPress.WP.I18n">
		<properties>
			<property name="text_domain" type="array" value="draggable-post-order"/>
		</properties>
	</rule>
	<rule ref="WordPress.WhiteSpace.ControlStructureSpacing">
		<properties>
			<property name="blank_line_check" value="true"/>
		</properties>
	</rule>

	<!-- Overwrite WP Rules -->
	<rule ref="Generic.Arrays.DisallowShortArraySyntax.Found">
		<severity>0</severity>
	</rule>
	<rule ref="Generic.Arrays.DisallowLongArraySyntax.Found" />
	<rule ref="WordPress.PHP.YodaConditions.NotYoda">
		<severity>0</severity>
	</rule>
	<rule ref="WordPress.PHP.DisallowShortTernary.Found">
		<severity>0</severity>
	</rule>
</ruleset>
