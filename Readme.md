# Draggable Post Order

## Features

- makes post types orderable in the admin screen via drag'n'drop with *jquery-ui-sortable*

## How to use

Add this to your **functions.php**

```php
add_post_type_support('your-custom-post-type', \Draggable_Post_Order\Draggable_Post_Order::$post_type_feature);
```

## License

[GPL v2](LICENSE)
