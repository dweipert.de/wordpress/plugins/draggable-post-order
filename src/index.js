/* global jQuery ajaxurl draggablePostOrder */

import './index.scss';

( function ( $ ) {
	const overlay = $( '<div class="post-order-overlay">' ).appendTo( 'body' );
	$( '#the-list' ).sortable( {
		items: 'tr',
		axis: 'y',
		cursor: 'move',

		// @see https://stackoverflow.com/a/57611747/6114451
		start( e, ui ) {
			const $originals = ui.helper.children();
			ui.placeholder.children().each( function ( index ) {
				$( this ).width( $originals.eq( index ).width() );
				$( this ).height( $originals.eq( index ).height() );
			} );
		},
		helper( e, tr ) {
			const $helper = tr.clone();
			const $originals = tr.children();
			$helper.children().each( function ( index ) {
				$( this ).width( $originals.eq( index ).outerWidth( true ) );
			} );
			return $helper;
		},

		update() {
			// show overlay
			overlay.addClass( 'post-order-overlay--visible' );

			// update posts
			$.post(
				ajaxurl,
				{
					action: 'update-post-order',
					nonce: draggablePostOrder.nonce,
					page: $( '#current-page-selector' ).val(),
					perPage: $( '.screen-per-page' ).val(),
					postOrder: $( '#the-list' ).sortable( 'serialize' ),
				},
				() => {
					// hide overlay after updating
					overlay.removeClass( 'post-order-overlay--visible' );
				}
			);
		},
	} );
} )( jQuery );
