<?php
/**
 * Functions for getting the correct adjacent post
 *
 * @package Draggable_Post_Order
 */

namespace Draggable_Post_Order;

/**
 * Class Adjacent_Post_Order
 *
 * @package Draggable_Post_Order
 */
class Adjacent_Post_Order {

	/**
	 * Initialize.
	 */
	public static function init() {
		add_filter( 'get_previous_post_join', [ self::class, 'get_post_join' ], 10, 5 );
		add_filter( 'get_next_post_join', [ self::class, 'get_post_join' ], 10, 5 );
		add_filter( 'get_previous_post_where', [ self::class, 'get_previous_post_where' ], 10, 5 );
		add_filter( 'get_next_post_where', [ self::class, 'get_next_post_where' ], 10, 5 );
		add_filter( 'get_previous_post_sort', [ self::class, 'get_post_sort' ], 10, 3 );
		add_filter( 'get_next_post_sort', [ self::class, 'get_post_sort' ], 10, 3 );
	}

	/**
	 * Callback for "get_previous_post_join" and "get_previous_post_join" filters.
	 * Joins in the postmeta table for further use.
	 *
	 * @param string  $join           The JOIN clause in the SQL.
	 * @param bool    $in_same_term   Whether post should be in a same taxonomy term.
	 * @param array   $excluded_terms Array of excluded term IDs.
	 * @param string  $taxonomy       Taxonomy. Used to identify the term used when `$in_same_term` is true.
	 * @param WP_Post $post           WP_Post object.
	 *
	 * @return string
	 */
	public static function get_post_join( $join, $in_same_term, $excluded_terms, $taxonomy, $post ) {
		if ( ! Draggable_Post_Order::supports( $post->post_type ) ) {
			return $join;
		}

		global $wpdb;

		$join .= " INNER JOIN {$wpdb->postmeta} as pm ON p.id = pm.post_id";

		return $join;
	}

	/**
	 * Helper function to set the WHERE clause.
	 *
	 * @param string  $where The `WHERE` clause in the SQL.
	 * @param WP_Post $post  WP_Post object.
	 * @param string  $op    The comparative operator to use.
	 *
	 * @return string
	 */
	private static function get_post_where( $where, $post, $op ) {
		if ( ! Draggable_Post_Order::supports( $post->post_type ) ) {
			return $where;
		}

		global $wpdb;

		$post_order = get_post_meta( $post->ID, Draggable_Post_Order::$meta_key, true );

		return $wpdb->prepare(
		    // phpcs:ignore WordPress.DB.PreparedSQL.InterpolatedNotPrepared
			"WHERE pm.meta_key = %s AND pm.meta_value $op %s AND p.post_type = %s AND p.post_status = 'publish'",
			Draggable_Post_Order::$meta_key,
			$post_order,
			$post->post_type
		);
	}

	/**
	 * Callback for "get_previous_post_where" filter.
	 *
	 * @param string  $where          The `WHERE` clause in the SQL.
	 * @param bool    $in_same_term   Whether post should be in a same taxonomy term.
	 * @param array   $excluded_terms Array of excluded term IDs.
	 * @param string  $taxonomy       Taxonomy. Used to identify the term used when `$in_same_term` is true.
	 * @param WP_Post $post           WP_Post object.
	 * @return string
	 */
	public static function get_previous_post_where( $where, $in_same_term, $excluded_terms, $taxonomy, $post ) {
		return self::get_post_where( $where, $post, '<' );
	}

	/**
	 * Callback for "get_next_post_where" filter.
	 *
	 * @param string  $where          The `WHERE` clause in the SQL.
	 * @param bool    $in_same_term   Whether post should be in a same taxonomy term.
	 * @param array   $excluded_terms Array of excluded term IDs.
	 * @param string  $taxonomy       Taxonomy. Used to identify the term used when `$in_same_term` is true.
	 * @param WP_Post $post           WP_Post object.
	 * @return string
	 */
	public static function get_next_post_where( $where, $in_same_term, $excluded_terms, $taxonomy, $post ) {
		return self::get_post_where( $where, $post, '>' );
	}

	/**
	 * Callback for "get_previous_post_sort" and "get_next_post_sort" filters.
	 *
	 * @param string  $order_by The `ORDER BY` clause in the SQL.
	 * @param WP_Post $post    WP_Post object.
	 * @param string  $order   Sort order. 'DESC' for previous post, 'ASC' for next.
	 *
	 * @return string
	 */
	public static function get_post_sort( $order_by, $post, $order ) {
		if ( ! Draggable_Post_Order::supports( $post->post_type ) ) {
			return $order_by;
		}

		return "ORDER BY pm.meta_value $order LIMIT 1";
	}
}
