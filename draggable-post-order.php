<?php
/**
 * Draggable Post Order
 *
 * @package Draggable_Post_Order
 */

/**
 * Plugin Name: Draggable Post Order
 * Description: Drag'n'drop posts to order them
 * Plugin URI: https://gitlab.com/DRogueRonin/wp-plugin-draggable-post-order
 * Author: Daniel Weipert
 * Version: 1.0.3
 * Author URI: https://dweipert.de
 * Text Domain: draggable-post-order
 */

require __DIR__ . '/vendor/autoload.php';

/**
 * Callback for "init" action.
 */
function draggable_post_order() {
	\Draggable_Post_Order\Draggable_Post_Order::init();
}
add_action( 'init', 'draggable_post_order' );

/**
 * URL to enqueue scripts and styles.
 *
 * @param string $path Relative path inside the build folder.
 *
 * @return string
 */
function draggable_post_order_assets_url( $path ) {
	return plugin_dir_url( __FILE__ ) . "build/$path";
}

/**
 * Absolute path to plugin folder.
 *
 * @return string
 */
function draggable_post_order_path() {
	return plugin_dir_path( __FILE__ );
}
