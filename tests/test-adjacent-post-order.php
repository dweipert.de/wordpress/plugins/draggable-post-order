<?php
/**
 * Class TestAdjacentPostOrder
 *
 * @package DraggablePostOrder
 */

use Draggable_Post_Order\Draggable_Post_Order;

/**
 * Sample test case.
 */
class TestAdjacentPostOrder extends WP_UnitTestCase {

	/**
	 * An array of posts for testing.
	 *
	 * @var WP_Post[]
	 */
	public array $test_posts = [];

	/**
	 * Runs the routine before each test is executed.
	 */
	public function setUp(): void {
		parent::setUp();

		add_post_type_support( 'post', Draggable_Post_Order::$post_type_feature );

		// create three posts with order.
		for ( $i = 1; $i <= 3; $i++ ) {
			$id = $this->factory()->post->create();
			update_post_meta( $id, Draggable_Post_Order::$meta_key, $i );

			$this->test_posts[] = get_post( $id );
		}

		// since tests are loaded after init, run init manually.
		Draggable_Post_Order::init();
	}

	/**
	 * Test previous and next post with creation order
	 */
	public function test_previous_next_post_creation_order() {
		$first  = $this->test_posts[0];
		$second = $this->test_posts[1];
		$third  = $this->test_posts[2];

		$this->go_to( '/?p=' . $second->ID );

		$this->assertEquals( $first->ID, get_previous_post()->ID );
		$this->assertEquals( $third->ID, get_next_post()->ID );
	}

	/**
	 * Test previous and next post jumbled order
	 */
	public function test_previous_next_post_jumbled_order() {
		$first  = $this->test_posts[1];
		$second = $this->test_posts[2];
		$third  = $this->test_posts[0];

		update_post_meta( $first->ID, Draggable_Post_Order::$meta_key, 1 );
		update_post_meta( $second->ID, Draggable_Post_Order::$meta_key, 2 );
		update_post_meta( $third->ID, Draggable_Post_Order::$meta_key, 3 );

		// test whether the key was set properly.
		$this->assertEquals( 2, get_post_meta( $second->ID, Draggable_Post_Order::$meta_key, true ) );

		$this->go_to( '/?p=' . $second->ID );

		$this->assertEquals( $first->ID, get_previous_post()->ID );
		$this->assertEquals( $third->ID, get_next_post()->ID );
	}
}
